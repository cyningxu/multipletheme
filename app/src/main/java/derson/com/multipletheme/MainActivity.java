package derson.com.multipletheme;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import derson.com.multipletheme.colorUi.util.ThemeModeUiUtil;
import derson.com.multipletheme.colorUi.widget.ThemeModeButton;


public class MainActivity extends BaseActivity {

    ThemeModeButton btn, btn_next;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btn = (ThemeModeButton) findViewById(R.id.btn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SharedPreferencesMgr.getInt("theme", 0) == 1) {
                    SharedPreferencesMgr.setInt("theme", 0);
                    setTheme(R.style.theme_1);
                } else {
                    SharedPreferencesMgr.setInt("theme", 1);
                    setTheme(R.style.theme_2);
                }
                final View rootView = getWindow().getDecorView();
                ThemeModeUiUtil.getInstance().changeTheme(rootView, getTheme());
            }
        });
        btn_next = (ThemeModeButton) findViewById(R.id.btn_2);
        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, SecondActivity.class));
            }
        });
    }
}
