package derson.com.multipletheme.colorUi.widget;

import android.content.Context;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ScrollView;

import derson.com.multipletheme.colorUi.ThemeModeUiInterface;
import derson.com.multipletheme.colorUi.util.ThemeModeUiBase;
import derson.com.multipletheme.colorUi.util.ThemeType;

public class ThemeModeScrollView extends ScrollView implements ThemeModeUiInterface {
    private final ThemeModeUiBase themeModeUiBase;

    public ThemeModeScrollView(Context context) {
        this(context, null);
    }

    public ThemeModeScrollView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ThemeModeScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        themeModeUiBase = new ThemeModeUiBase(this, attrs);
    }

    @Override
    public View getView() {
        return this;
    }

    @Override
    public void setTheme(ThemeType themeType, Resources.Theme themeId) {
        themeModeUiBase.setTheme(themeId);
    }

    @Override
    public void onThemeChanged(ThemeType themeType, Resources.Theme themeId) {

    }
}
