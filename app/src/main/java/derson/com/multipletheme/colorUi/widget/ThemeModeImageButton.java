package derson.com.multipletheme.colorUi.widget;

import android.content.Context;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageButton;

import derson.com.multipletheme.colorUi.ThemeModeUiInterface;
import derson.com.multipletheme.colorUi.util.ThemeModeUiBase;
import derson.com.multipletheme.colorUi.util.ThemeType;

public class ThemeModeImageButton extends ImageButton implements ThemeModeUiInterface {
    private final ThemeModeUiBase themeModeUiBase;

    public ThemeModeImageButton(Context context) {
        this(context, null);
    }

    public ThemeModeImageButton(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ThemeModeImageButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        themeModeUiBase = new ThemeModeUiBase(this, attrs);
    }

    @Override
    public View getView() {
        return this;
    }

    @Override
    public void setTheme(ThemeType themeType, Resources.Theme themeId) {
        themeModeUiBase.setTheme(themeId);
    }

    @Override
    public void onThemeChanged(ThemeType themeType, Resources.Theme themeId) {

    }
}
