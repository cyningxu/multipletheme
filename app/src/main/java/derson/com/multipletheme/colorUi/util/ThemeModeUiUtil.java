package derson.com.multipletheme.colorUi.util;

import android.content.res.Resources;
import android.content.res.TypedArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import derson.com.multipletheme.R;
import derson.com.multipletheme.colorUi.ThemeModeUiInterface;

public class ThemeModeUiUtil {
    private static ThemeModeUiUtil instance = null;
    private int[] attrs = new int[]{R.attr.themeId};
    private ThemeType themeType = ThemeType.DAY;

    public static ThemeModeUiUtil getInstance() {
        if (instance == null) {
            synchronized (ThemeModeUiUtil.class) {
                if (instance == null) {
                    instance = new ThemeModeUiUtil();
                }
            }
        }
        return instance;
    }

    public ThemeType getThemeType() {
        return themeType;
    }

    public void changeTheme(View rootView, Resources.Theme theme) {
        TypedArray typedArray = theme.obtainStyledAttributes(attrs);
        switch (typedArray.getInteger(0, 1)) {
            case 1:
                themeType = ThemeType.DAY;
                break;
            case 2:
                themeType = ThemeType.NIGHT;
                break;
            default:
                themeType = ThemeType.PIC;
                break;
        }
        typedArray.recycle();
        changeThemeRecursion(rootView, theme);
    }

    private void changeThemeRecursion(View rootView, Resources.Theme theme) {
        if (rootView instanceof ThemeModeUiInterface) {
            ((ThemeModeUiInterface) rootView).setTheme(themeType, theme);
            if (rootView instanceof ViewGroup) {
                int count = ((ViewGroup) rootView).getChildCount();
                for (int i = 0; i < count; i++) {
                    changeThemeRecursion(((ViewGroup) rootView).getChildAt(i), theme);
                }
            }
            if (rootView instanceof AbsListView) {
                try {
                    Field localField = AbsListView.class.getDeclaredField("mRecycler");
                    localField.setAccessible(true);
                    Method localMethod = Class.forName("android.widget.AbsListView$RecycleBin").getDeclaredMethod("clear");
                    localMethod.setAccessible(true);
                    localMethod.invoke(localField.get(rootView));
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
            ((ThemeModeUiInterface) rootView).onThemeChanged(themeType, theme);
        } else {
            if (rootView instanceof ViewGroup) {
                int count = ((ViewGroup) rootView).getChildCount();
                for (int i = 0; i < count; i++) {
                    changeThemeRecursion(((ViewGroup) rootView).getChildAt(i), theme);
                }
            }
            if (rootView instanceof AbsListView) {
                try {
                    Field localField = AbsListView.class.getDeclaredField("mRecycler");
                    localField.setAccessible(true);
                    Method localMethod = Class.forName("android.widget.AbsListView$RecycleBin").getDeclaredMethod("clear");
                    localMethod.setAccessible(true);
                    localMethod.invoke(localField.get(rootView));
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        }
    }


}
