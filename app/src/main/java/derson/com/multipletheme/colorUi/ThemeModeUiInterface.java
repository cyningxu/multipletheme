package derson.com.multipletheme.colorUi;

import android.content.res.Resources;
import android.view.View;

import derson.com.multipletheme.colorUi.util.ThemeType;

public interface ThemeModeUiInterface {


    public View getView();

    public void setTheme(ThemeType themeType, Resources.Theme themeId);

    /**
     * called when the theme of itself and it's children has changed.
     * <p>it's a better time to changed the theme of itself</p>
     * <p>attention!</p>
     * do not change the theme of it's parent
     *
     * @param themeType
     * @param themeId
     */
    public void onThemeChanged(ThemeType themeType, Resources.Theme themeId);
}
